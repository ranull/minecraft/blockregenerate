package com.rngservers.blockregenerate.block;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;

import com.rngservers.blockregenerate.Main;

public class BlockManager {
	private Main plugin;

	Map<Location, Map<Material, Integer>> blockList = new HashMap<Location, Map<Material, Integer>>();

	public BlockManager(Main plugin) {
		this.plugin = plugin;
	}

	public void secondTimer() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Iterator<Entry<Location, Map<Material, Integer>>> iterator = blockList.entrySet()
						.iterator(); iterator.hasNext();) {
					Entry<Location, Map<Material, Integer>> entry = iterator.next();

					// Block data
					Location location = entry.getKey();
					Material material = getMaterial(location);
					Integer duration = getDuration(location) - 1;

					recordBlock(location, material, duration);
					if (duration <= 0) {
						if (!plugin.getConfig().getBoolean("settings.unloadedChunks")) {
							if (!location.getWorld().isChunkLoaded(location.getChunk())) {
								continue;
							}
						}
						location.getBlock().setType(material);
						iterator.remove();
					}
				}
			}
		}.runTaskTimer(plugin, 0L, 20L);
	}

	public void recordBlock(Location location, Material material, Integer duration) {
		if (duration == null) {
			return;
		}
		Map<Material, Integer> innerMap = new HashMap<Material, Integer>();
		innerMap.put(material, duration);
		blockList.put(location, innerMap);
	}

	public Material getMaterial(Location location) {
		Iterator<Entry<Material, Integer>> iterator = blockList.get(location).entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Material, Integer> entry = iterator.next();
			return entry.getKey();
		}
		return null;
	}

	public Integer getDuration(Location location) {
		Iterator<Entry<Material, Integer>> iterator = blockList.get(location).entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Material, Integer> entry = iterator.next();
			return entry.getValue();
		}
		return null;
	}

	public Integer getGenerateDelay(Material material) {
		try {
			Integer delay = Integer
					.parseInt(plugin.getConfig().getString("blocks." + material.toString() + ".generateDelay"));
			return delay;
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public Integer getReplaceDelay(Material material) {
		try {
			Integer delay = Integer
					.parseInt(plugin.getConfig().getString("blocks." + material.toString() + ".replaceDelay"));
			return delay;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public Material getReplace(Material material) {
		String replace = plugin.getConfig().getString("blocks." + material.toString() + ".replace");
		if (replace != null) {
			return Material.getMaterial(replace);
		}
		return null;
	}

	public Map<Location, Map<Material, Integer>> getBlockList() {
		return blockList;
	}

	public void setBlockList(Map<Location, Map<Material, Integer>> blockList) {
		this.blockList = blockList;
	}

	public Map<Location, Map<Material, Integer>> getBlocks() {
		return this.blockList;
	}
}
