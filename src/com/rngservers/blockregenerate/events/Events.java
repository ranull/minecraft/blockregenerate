package com.rngservers.blockregenerate.events;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.rngservers.blockregenerate.Main;
import com.rngservers.blockregenerate.block.BlockManager;

public class Events implements Listener {
	private Main plugin;
	private BlockManager block;

	public Events(Main plugin, BlockManager block) {
		this.plugin = plugin;
		this.block = block;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Location location = event.getBlock().getLocation();
		Material material = event.getBlock().getType();
		if (plugin.worldGuard() != null) {
			List<String> regionList = plugin.worldGuard().getRegion(location);
			String allowedRegion = plugin.getConfig().getString("blocks." + material + ".region");
			if (allowedRegion != null) {
				if (!regionList.contains(allowedRegion.toLowerCase())) {
					return;
				}
			}
		}
		if (!player.hasPermission("blockregenerate.use")) {
			return;
		}
		if (player.getGameMode().equals(GameMode.CREATIVE)) {
			return;
		}
		if (player.getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH)) {
			Boolean silk = plugin.getConfig().getBoolean("settings.allowSilkTouch");
			if (silk != null && silk.equals(false)) {
				return;
			}
		}
		if (plugin.getConfig().getString("blocks." + material.toString()) == null) {
			return;
		}
		if (plugin.getConfig().getBoolean("blocks." + material.toString() + ".denyDrop")) {
			event.setDropItems(false);
		}
		if (plugin.getConfig().getBoolean("blocks." + material.toString() + ".denyEXP")) {
			event.setExpToDrop(0);
		}

		Integer duration = block.getGenerateDelay(material);
		if (duration != null) {
			block.recordBlock(location, material, duration);
		}

		// Replace block
		Material replace = block.getReplace(material);
		Integer replaceDelay = block.getReplaceDelay(material);
		if (replace != null) {
			new BukkitRunnable() {
				@Override
				public void run() {
					event.getBlock().setType(replace);
				}
			}.runTaskLater(plugin, replaceDelay);
		}
	}
}
