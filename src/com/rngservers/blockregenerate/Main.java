package com.rngservers.blockregenerate;

import org.bukkit.plugin.java.JavaPlugin;

import com.rngservers.blockregenerate.block.BlockManager;
import com.rngservers.blockregenerate.commands.BlockRegenerate;
import com.rngservers.blockregenerate.data.DataManager;
import com.rngservers.blockregenerate.events.Events;
import com.rngservers.blockregenerate.hooks.WorldGuardSupport;

public class Main extends JavaPlugin {
	private DataManager data;
	private BlockManager block;
	private WorldGuardSupport worldGuard;

	@Override
	public void onEnable() {
		if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
			worldGuard = new WorldGuardSupport();
		}
		worldGuard();
		saveDefaultConfig();
		block = new BlockManager(this);
		data = new DataManager(this, block);
		data.createDataFile();
		if (getConfig().getBoolean("settings.saveData")) {
			data.loadBlockData();
		}
		block.secondTimer();

		this.getCommand("blockregenerate").setExecutor(new BlockRegenerate(this));
		this.getServer().getPluginManager().registerEvents(new Events(this, block), this);
	}

	@Override
	public void onDisable() {
		if (getConfig().getBoolean("settings.saveData")) {
			data.saveBlockData(block.getBlockList());
		}
	}

	public WorldGuardSupport worldGuard() {
		return worldGuard;
	}
}