package com.rngservers.blockregenerate.hooks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;

public class WorldGuardSupport {
	public List<String> getRegion(Location location) {
		RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
		BukkitWorld world = new BukkitWorld(location.getWorld());
		RegionManager regions = container.get(world);
		if (regions == null) {
			return null;
		}
		BlockVector3 vector3 = BlockVector3.at(location.getX(), location.getY(), location.getZ());
		ApplicableRegionSet applicableRegions = regions.getApplicableRegions(vector3);
		List<String> regionList = new ArrayList<String>();
		for (ProtectedRegion region : applicableRegions.getRegions()) {
			regionList.add(region.getId().toLowerCase());
		}
		return regionList;
	}
}
