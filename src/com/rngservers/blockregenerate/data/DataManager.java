package com.rngservers.blockregenerate.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.rngservers.blockregenerate.Main;
import com.rngservers.blockregenerate.block.BlockManager;

public class DataManager {
	private Main plugin;
	private BlockManager block;
	private FileConfiguration data;
	private File dataFile;

	public DataManager(Main plugin, BlockManager block) {
		this.plugin = plugin;
		this.block = block;
	}

	public void loadData() throws FileNotFoundException, IOException, InvalidConfigurationException {
		data.load(dataFile);
	}

	public FileConfiguration getData() {
		return this.data;
	}

	public void saveBlockData(Map<Location, Map<Material, Integer>> blockList) {
		Iterator<Entry<Location, Map<Material, Integer>>> iterator = blockList.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Location, Map<Material, Integer>> entry = iterator.next();

			Location location = entry.getKey();
			String world = location.getWorld().getName();
			int x = location.getBlockX();
			int y = location.getBlockY();
			int z = location.getBlockZ();
			Material material = block.getMaterial(location);
			Integer duration = block.getDuration(location);

			data.set(world + "." + x + "_" + y + "_" + z + ".material", material.toString());
			data.set(world + "." + x + "_" + y + "_" + z + ".duration", duration);
			saveData();
		}
	}

	public void loadBlockData() {
		Map<Location, Map<Material, Integer>> blockList = new HashMap<Location, Map<Material, Integer>>();
		for (String world : data.getKeys(false)) {
			for (String block : data.getConfigurationSection(world).getKeys(false)) {
				Material material = Material.getMaterial(data.getString(world + "." + block + ".material"));
				Integer duration = data.getInt(world + "." + block + ".duration");

				String[] cords = block.split("_");
				int x = Integer.valueOf(cords[0]);
				int y = Integer.valueOf(cords[1]);
				int z = Integer.valueOf(cords[2]);
				Location location = new Location(plugin.getServer().getWorld(world), x, y, z);

				Map<Material, Integer> innerMap = new HashMap<Material, Integer>();
				innerMap.put(material, duration);

				blockList.put(location, innerMap);
				data.set(world + "." + block, null);
			}
		}
		block.setBlockList(blockList);
		saveData();
	}

	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createDataFile() {
		dataFile = new File(plugin.getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			try {
				dataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
}
