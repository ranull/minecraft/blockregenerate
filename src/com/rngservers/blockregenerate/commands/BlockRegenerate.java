package com.rngservers.blockregenerate.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.rngservers.blockregenerate.Main;

public class BlockRegenerate implements CommandExecutor {
	private Main plugin;

	public BlockRegenerate(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String version = "1.2";
		String author = "RandomUnknown";

		if (args.length < 1) {
			sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GOLD + "BlockRegenerate " + ChatColor.GRAY
					+ ChatColor.GRAY + "v" + version);
			sender.sendMessage(ChatColor.GRAY + "/blockregenerate " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
					+ " Plugin info");
			if (sender.hasPermission("blockregenerate.reload")) {
				sender.sendMessage(ChatColor.GRAY + "/blockregenerate reload " + ChatColor.DARK_GRAY + "-"
						+ ChatColor.RESET + " Reload plugin");
			}
			sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
			return true;
		}
		if (args.length == 1 && args[0].equals("reload")) {
			if (!sender.hasPermission("blockregenerate.reload")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "BlockRegenerate" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " No Permission!");
				return true;
			}
			plugin.reloadConfig();
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "BlockRegenerate" + ChatColor.DARK_GRAY
					+ "]" + ChatColor.RESET + " Reloaded config file!");
		}
		return true;
	}
}